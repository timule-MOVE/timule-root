#!/bin/sh
now=$(date +"%T")
echo $now >> /volumio/updates/log.txt
echo "This is the update file if it exists" >> /volumio/updates/log.txt

echo "Start unzip" >> /volumio/updates/log.txt
unzip -o /volumio/updates/timule_0_03.zip >> /volumio/updates/log.txt

echo "Start copy songs" >> /volumio/updates/log.txt
\mv /volumio/updates/timule_0_03/INTERNAL/* /data/INTERNAL/
echo "Start copy playlists..." >> /volumio/updates/log.txt
\mv /volumio/updates/timule_0_03/playlist/* /data/playlist/
echo "End copy..." >> /volumio/updates/log.txt
#rm -r /volumio/updates/timule_0_03.zip
echo "Delete zip..." >> /volumio/updates/log.txt
#rm -r /volumio/updates/timule_0_03
echo "delete folder..." >> /volumio/updates/log.txt
#rm -- "$0"
echo "deleted script..." >> /volumio/updates/log.txt
echo "before reboot..." >> /volumio/updates/log.txt

#sudo reboot
