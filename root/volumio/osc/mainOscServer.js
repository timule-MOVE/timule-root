var bedName="MOVE010000";
var softwareVersion="1.0.1";
var bedIp="";
var appIp="";
var bedIsBusy="available"; //"available", "busy"
var osc = require('node-osc');
var ip = require("ip");
console.dir("PI adddress "+ip.address())
console.log("start listening");
var host = require("os");
var fs = require("fs");
var hostName=host.hostname().toUpperCase()
console.log("hostname "+hostName);
var io2 = require('socket.io-client');
var socket = io2.connect('http://localhost:3000');
var Client = require('node-rest-client').Client;
var client = new Client();

var oscServer = new osc.Server(3333, '0.0.0.0');

var readStatus =fs.readFileSync("/volumio/osc/connected.txt").toString().substring(0,1);
console.log("Read file: "+(readStatus=="1"));


// Bed available on startup
var sys = require('sys')
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { console.log(stdout) }
var code ="echo "+0+" > /volumio/osc/connected.txt";
exec(code, puts);



oscServer.on("message", function (msg, rinfo) {
      console.log("TUI message:");
      console.log(msg);
      bedIp=ip.address();
      appIp=rinfo['address']

      
      

      if(msg[0]=="/setWireless"){
            console.log("Should change wifi");
            //var ssidVal=""+msg[1].split(' ')[0];
            //var encriptVal=""+msg[1].split(' ')[1];
            //var psswVal=""+msg[1].split(' ')[2];
            var ssidVal="ArmstrongLiberado";
            var encriptVal="WPA2";
            var psswVal="armstrong";
            console.log("Ssid "+ssidVal);
            console.log("encript "+encriptVal);
            console.log("pass "+psswVal);
            var data = {
                  ssid: ssidVal,
                  encryption: encriptVal,
                  password: psswVal
            };
            
            console.log("WIFI settings new: \n"+data['password']);
            socket.emit('saveWirelessNetworkSettings', data);
      }

      if(msg[0]=="/exec"){
            console.log("Should run code");
            code=msg[1];
            var sys = require('sys')
            var exec = require('child_process').exec;
            function puts(error, stdout, stderr) { sys.puts(stdout) }
            exec(code, puts);
      }

      if(msg[0]=="/chooseSession"){
            console.log("Should play session number choosed");
            var restApiCall ="http://localhost:3000/api/v1/commands/?cmd=playplaylist&name="
            var playListName=msg[1];
            client.get(restApiCall+""+playListName, function (data, response) {
            // parsed response body as js object 
            console.log(data);
            // raw response 
            console.log(response);
            });
      }

      if(msg[0]=="/checkupdate" ){
            console.log("Checking update");
            var sys = require('sys')
            var exec = require('child_process').exec;
            function puts(error, stdout, stderr) { console.log(stdout) }
            var code ="sh /home/volumio/update-fw.sh";
            exec(code, puts);

      }

      if(msg[0]=="/bedbusy" ){
            console.log("Set bed busy "+msg[1]);
            bedIsBusy=msg[1]; 
            var con=0;
            if(bedIsBusy=="busy"){
                  console.log("Set busy");
                  con =1;
            }else if(bedIsBusy=="available") {
                  console.log("Set available");
                  con=0
            }
            var sys = require('sys')
            var exec = require('child_process').exec;
            function puts(error, stdout, stderr) { console.log(stdout) }
            var code ="echo "+con+" > /volumio/osc/connected.txt";
            exec(code, puts);
      }

      if(msg[0]=="/play" && msg[1]=="1"){
            console.log("Should emit play");
           socket.emit('play');
      }

      if(msg[0]=="/pause" && msg[1]=="1"){
            console.log("Should emit pause");
            socket.emit('pause');
      }

      if(msg[0]=="/stop" && msg[1]=="1"){
            console.log("Should emit stop");
            socket.emit('stop');
      }

      if(msg[0]=="/volumeGeneral"){
            var restApiCall ="http://localhost:3000/api/v1/commands/?cmd=volume&volume="
            var volumeValue=msg[1];
            client.get(restApiCall+""+volumeValue, function (data, response) {
            // parsed response body as js object 
            console.log(data);
            // raw response 
            console.log(response);
            });
      }

      if(msg[0]=="/changePosition"){
            var seekVal = msg[1];
            console.log("Should emit seek "+seekVal);
            socket.emit('seek',seekVal);
      }

      //    socket.emit('seek',''+sk);
//    sk=sk+1;

      var oscclient = new osc.Client(appIp, 6000);
      //console.log("Start sending from: "+bedIp+" to "+appIp);
      var bedIsBusyText="avali";
       readStatus =fs.readFileSync("/volumio/osc/connected.txt").toString().substring(0,1);
        console.log("Read file: "+(readStatus=="1"));


      if(readStatus=="0"){
            bedIsBusyText="available";
      }else{
            bedIsBusyText="busy";
      }
      console.log("Read status"+readStatus);

      console.log("Send read file "+bedIsBusyText);

      var sendMsg=""+bedIp+"-"+hostName +"-"+softwareVersion+"-"+bedIsBusyText;
      //console.log("\tSending message: "+sendMsg);

      oscclient.send('/ipbed',sendMsg, function (err) {
            console.log("\tSent message "+sendMsg);
            if (err) {
            console.error(new Error(err));
            }
            oscclient.kill();
      });

});
