'use strict';

var libQ = require('kew');
var fs = require('fs-extra');
var api = require('/volumio/http/restapi.js');
var bodyParser = require('body-parser');

module.exports = interfaceApi;

function interfaceApi(context) {
    var self = this;

    self.context = context;
    self.commandRouter = self.context.coreCommand;
    self.musicLibrary = self.commandRouter.musicLibrary;
    var notFound = {'Error': "Error 404: resource not found"};
    var success = {'Message': "Succesfully restored resource"};

    self.logger = self.commandRouter.logger;

    api.route('/backup/playlists/:type')
        .get(function (req, res) {

            var type = {'type': req.params.type};

            var response = self.commandRouter.loadBackup(type);

            if (response._data != undefined)
                res.json(response._data);
            else
                res.json(notFound);
        });

    api.route('/backup/config/')
        .get(function (req, res) {
            var response = self.commandRouter.getPluginsConf();

            if (response != undefined)
                res.json(response);
            else
                res.json(notFound);
        });


    api.route('/restore/playlists/')
        .post(function (req, res) {
            var response = {'Error': "Error: impossible to restore given data"};

            try{
                self.commandRouter.restorePlaylist({'type': req.body.type, 'backup': JSON.parse(req.body.data)});
                res.json(success);
            }catch(e){
                res.json(response);
            }
        });


    api.route('/restore/config/')
        .post(function (req, res) {
            var response = {'Error': "Error: impossible to restore configurations"};

            try{
                var bobby = JSON.parse(req.body.config);
                self.commandRouter.restorePluginsConf(JSON.parse(req.body.config));
                res.json(success);
            }catch(e){
                res.json(response);
            }
        });

    api.route('/commands')
        .get(function (req, res) {
            var response = {'Error': "Error: impossible to execute command"};

            try{
                if(req.query.cmd == "play"){
                    var timeStart = Date.now();
                    if (req.query.N == undefined) {
                        self.logStart('Client requests Volumio play')
                            .then(self.commandRouter.volumioPlay.bind(self.commandRouter))
                            .fail(self.pushError.bind(self))
                            .done(function () {
                                res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                            });
                    } else {
                        var N = parseInt(req.query.N);
                        self.logStart('Client requests Volumio play at index '+ N)
                            .then(self.commandRouter.volumioPlay.bind(self.commandRouter,N))
                            .done(function () {
                                res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                            });
                    }
                }
                else if (req.query.cmd == "toggle"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Volumio toggle')
                        .then(self.commandRouter.volumioToggle.bind(self.commandRouter))
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if (req.query.cmd == "stop"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Volumio stop')
                        .then(self.commandRouter.volumioStop.bind(self.commandRouter))
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                /////////////////////////////////////////////////////////7
                // Begin Cristian's code
                }else if(req.query.cmd == "wifiChange"){
                    var wifi_ssid = req.query.ssid;
                    var wifi_pass = req.query.pass;
                    var wifi_enc = req.query.enc;
                    //var code="node /volumio/app/setupWirelessNetwork.js "+wifi_ssid+" "+wifi_pass+" "+wifi_enc;
                    var timeStart = Date.now();
                    var data = {
                        ssid: wifi_ssid,
                        encryption: wifi_enc,
                        password: wifi_pass
                    };
                    self.logStart('\n\n!!!!!!!!!!!!\nClient requests Change in wifi '+wifi_ssid+' '+wifi_pass+' '+wifi_enc+"\n")
                        .then(function () {
                            //return self.commandRouter.playPlaylist.call(self.commandRouter,playlistName);
                            //var sys = require('sys')
                            //var exec = require('child_process').exec;
                            //function puts(error, stdout, stderr) { sys.puts(stdout) }
                            //exec(code, puts);
                            var io = require('socket.io-client');
                            var socket = io.connect('http://localhost:3000');
                            socket.emit('saveWirelessNetworkSettings', data);
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':'wifi changed: '+data + " Success"});
                        });
                }
                else if (req.query.cmd == "getListWifi"){
                    var timeStart = Date.now();
                    self.logStart('Client requests list of WIFI')
                        .then(self.commandRouter.volumioStop.bind(self.commandRouter))
                        .fail(self.pushError.bind(self))
                        .done(function () {

                            var exec = require('child_process').exec;
                            var execSync = require('child_process').execSync;
                            
                            console.log('Cannot use regular scanning, forcing with ap-force')
                            var networksarray = [];
                            var arraynumber = 0;
                            
                            try {
                                var wirelessnets = execSync("/usr/bin/sudo /sbin/iw dev wlan0 scan ap-force", {encoding: 'utf8'});
                                var wirelessnets2 = wirelessnets.split('(on wlan0)');
                            
                                for (var i = 0; i < wirelessnets2.length; i++) {
                                    var network = {};
                                    var wirelessnets3 = wirelessnets2[i].split("\n")
                                    for (var e = 0; e < wirelessnets3.length; e++) {
                                        var scanResults = wirelessnets3[e].replace('\t', '').replace(' ', '').split(":");
                                        //console.log(scanResults);
                            
                                        if (scanResults[1]) {
                                            if ((scanResults[1].indexOf('CCMP') || scanResults[1].indexOf('TKIP')) >= 0) {
                                                network.security = 'wpa2';
                                            }
                                        }
                            
                                        switch (scanResults[0]) {
                                            case 'SSID':
                                                network.ssid = scanResults[1].toString();
                            
                                                break;
                                            case 'WPA':
                                                network.security = 'wpa2';
                            
                                                break;
                                            case 'signal':
                            
                                                var signal = '';
                                                var dbmraw = scanResults[1].split('.');
                                                var dbm = Number(dbmraw[0]);
                                                var rel = 100 + dbm;
                            
                                                if (rel >= 65)
                                                    signal = 5;
                                                else if (rel >= 50)
                                                    signal = 4;
                                                else if (rel >= 40)
                                                    signal = 3;
                                                else if (rel >= 30)
                                                    signal = 2;
                                                else if (rel >= 1)
                                                    signal = 1;
                            
                                                network.signal = signal;
                            
                                                break;
                                            default:
                                                break;
                                        }
                            
                                    }
                            
                                    if (network.ssid) {
                                        //console.log(network)
                                        if (networksarray.length > 0) {
                                            var found = false;
                                            for (var o = 0; o < networksarray.length; o++) {
                                                if (network.ssid == networksarray[o].ssid) {
                                                    found = true;
                                                }
                                            }
                                            if (found === false) {
                                                networksarray.push(network);
                                            }
                                        } else {
                                            networksarray.push(network);
                                        }
                                    }
                                }
                            
                                var networkresults = {"available": networksarray}
                                console.log(networkresults);
                                
                            } catch (e)
                            {console.log('Cannot use fallback scanning method: '+e)}
                            
                            res.json(networkresults);
                        });
                }
                // End Cristian's code
                /////////////////////////////////////////////////////////7

                else if (req.query.cmd == "pause"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Volumio pause')
                        .then(self.commandRouter.volumioPause.bind(self.commandRouter))
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if (req.query.cmd == "clearQueue"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Volumio Clear Queue')
                        .then(self.commandRouter.volumioClearQueue.bind(self.commandRouter))
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "prev"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Volumio previous')
                        .then(self.commandRouter.volumioPrevious.bind(self.commandRouter))
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "next"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Volumio next')
                        .then(self.commandRouter.volumioNext.bind(self.commandRouter))
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "volume"){
                    var VolumeInteger = req.query.volume;
                    if (VolumeInteger == "plus") {
                        VolumeInteger = '+';
                    } else if (VolumeInteger == "minus"){
                        VolumeInteger = '-';
                    }
                    else if (VolumeInteger == "mute" || VolumeInteger == "unmute" || VolumeInteger == "toggle") {

                    } else {
                        VolumeInteger = parseInt(VolumeInteger);
                    }

                    var timeStart = Date.now();
                    self.logStart('Client requests Volume ' + VolumeInteger)
                        .then(function () {
                            return self.commandRouter.volumiosetvolume.call(self.commandRouter,
                                VolumeInteger);
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "vibration"){
                    var VolumeInteger = req.query.volume;
                    if (VolumeInteger == "plus") {
                        VolumeInteger = '+';
                    } else if (VolumeInteger == "minus"){
                        VolumeInteger = '-';
                    }
                    else if (VolumeInteger == "mute" || VolumeInteger == "unmute" || VolumeInteger == "toggle") {

                    } else {
                        VolumeInteger = parseInt(VolumeInteger);
                    }

                    var timeStart = Date.now();
                    self.logStart('Client requests Vibration Volume ' + VolumeInteger)
                        .then(function () {
                            return self.commandRouter.volumiosetvibration.call(self.commandRouter,
                                VolumeInteger);
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
           
                else if(req.query.cmd == "playplaylist"){
                    var playlistName = req.query.name;
                    var timeStart = Date.now();
                    self.logStart('Client requests Volumio Play Playlist '+playlistName)
                        .then(function () {
                            return self.commandRouter.playPlaylist.call(self.commandRouter,
                                playlistName);
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd=="seek"){
                    var position = req.query.position;
                    if(position == "plus") {
                        position = '+';
                    }
                    else if (position == "minus"){
                        position = '-';
                    }
                    else {
                        position = parseInt(position);
                    }

                    var timeStart = Date.now();
                    self.logStart('Client requests Position ' + position)
                        .then(function () {
                            return self.commandRouter.volumioSeek(position);
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "repeat"){
                    var value = req.query.value;
                    if(value == "true"){
                        value = true;
                    }
                    else if (value == "false"){
                        value = false;
                    }

                    var timeStart = Date.now();
                    self.logStart('Client requests Repeat ' + value)
                        .then(function () {
                            if(value != undefined) {
                                return self.commandRouter.volumioRepeat(value, false);
                            }
                            else{
                                return self.commandRouter.repeatToggle();
                            }
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "random"){
                    var value = req.query.value;
                    if(value == "true"){
                        value = true;
                    }
                    else if (value == "false"){
                        value = false;
                    }

                    var timeStart = Date.now();
                    self.logStart('Client requests Random ' + value)
                        .then(function () {
                            if(value != undefined) {
                                return self.commandRouter.volumioRandom(value);
                            }
                            else{
                                return self.commandRouter.randomToggle();
                            }
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "startAirplay"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Start Airplay metadata parsing')
                        .then(function () {
                            self.commandRouter.executeOnPlugin('music_service', 'airplay_emulation', 'startAirplayMeta', '');
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else if(req.query.cmd == "stopAirplay"){
                    var timeStart = Date.now();
                    self.logStart('Client requests Start Airplay metadata parsing')
                        .then(function () {
                            self.commandRouter.executeOnPlugin('music_service', 'airplay_emulation', 'airPlayStop', '');
                        })
                        .fail(self.pushError.bind(self))
                        .done(function () {
                            res.json({'time':timeStart, 'response':req.query.cmd + " Success"});
                        });
                }
                else{
                    res.json({'Error': "command not recognized"});
                }
            } catch(e){
                self.commandRouter.logger.info("Error executing command");
                res.json(response);
            }
        });

    api.use('/v1', api);
    api.use(bodyParser.json());

    api.route('/getstate')
        .get(function (req, res) {


            var response = self.commandRouter.volumioGetState();

            if (response != undefined)
                res.json(response);
            else
                res.json(notFound);
        });

    api.route('/listplaylists')
        .get(function (req, res) {

            var response = self.commandRouter.playListManager.listPlaylist();

            var response = self.commandRouter.playListManager.listPlaylist();
            response.then(function (data) {
                if (data != undefined) {
                    res.json(data);
                } else {
                    res.json(notFound);
                }
            });



        });

}

// Receive console messages from commandRouter and broadcast to all connected clients
interfaceApi.prototype.printConsoleMessage = function (message) {
    var self = this;

    return libQ.resolve();
};

// Receive player queue updates from commandRouter and broadcast to all connected clients
interfaceApi.prototype.pushQueue = function (queue, connWebSocket) {
    var self = this;
    self.commandRouter.pushConsoleMessage('[' + Date.now() + '] ' + 'interfaceApi::pushQueue');

};

// Push the library root
interfaceApi.prototype.pushLibraryFilters = function (browsedata, connWebSocket) {
    var self = this;
    self.commandRouter.pushConsoleMessage('[' + Date.now() + '] ' + 'interfaceApi::pushLibraryFilters');
};

// Receive music library data from commandRouter and send to requester
interfaceApi.prototype.pushLibraryListing = function (browsedata, connWebSocket) {
    var self = this;
    self.commandRouter.pushConsoleMessage('[' + Date.now() + '] ' + 'interfaceApi::pushLibraryListing');
};

// Push the playlist view
interfaceApi.prototype.pushPlaylistIndex = function (browsedata, connWebSocket) {
    var self = this;
    self.commandRouter.pushConsoleMessage('[' + Date.now() + '] ' + 'interfaceApi::pushPlaylistIndex');

};

interfaceApi.prototype.pushMultiroom = function (selfConnWebSocket) {
    var self = this;
    //console.log("pushMultiroom 2");
    var volumiodiscovery = self.commandRouter.pluginManager.getPlugin('system_controller', 'volumiodiscovery');
}


// Receive player state updates from commandRouter and broadcast to all connected clients
interfaceApi.prototype.pushState = function (state, connWebSocket) {
    var self = this;
    self.commandRouter.pushConsoleMessage('[' + Date.now() + '] ' + 'interfaceApi::pushState');
};


interfaceApi.prototype.printToastMessage = function (type, title, message) {
    var self = this;

};

interfaceApi.prototype.broadcastToastMessage = function (type, title, message) {
    var self = this;

};

interfaceApi.prototype.pushMultiroomDevices = function (msg) {
};

interfaceApi.prototype.logDone = function (timeStart) {
    var self = this;
    self.commandRouter.pushConsoleMessage('[' + Date.now() + '] ' + '------------------------------ ' + (Date.now() - timeStart) + 'ms');
    return libQ.resolve();
};

interfaceApi.prototype.logStart = function (sCommand) {
    var self = this;
    self.commandRouter.pushConsoleMessage('\n' + '[' + Date.now() + '] ' + '---------------------------- ' + sCommand);
    return libQ.resolve();
};

// Pass the error if we don't want to handle it
interfaceApi.prototype.pushError = function (error) {
    if ((typeof error) === 'string') {
        return this.commandRouter.pushConsoleMessage.call(this.commandRouter, 'Error: ' + error);
    } else if ((typeof error) === 'object') {
        return this.commandRouter.pushConsoleMessage.call(this.commandRouter, 'Error:\n' + error.stack);
    }
    // Return a resolved empty promise to represent completion
    return libQ.resolve();
};

interfaceApi.prototype.pushAirplay = function (value) {
    this.logger.debug("Pushing airplay mode: s" + value);
};

interfaceApi.prototype.emitFavourites = function (value) {
    var self = this;

    self.logger.info("Pushing Favourites " + JSON.stringify(value));
};

interfaceApi.prototype.broadcastMessage = function(emit,payload) {

};
