# Setting Volumio on RPi with Audio Injector Support

## Volumio image

**Download latest image from [here](https://volumio.org/get-started/)**

This setup was verified with:

```
VERSION: 2.246
RELEASE DATE: 31-07-2017
IMAGE MD5: d947e500176dc54313f26c82eba6e48f 
```

TODO:
If you can't find the correct version, there is a copy of it [**here**]() 

* Use Etcher (with GUI) or dd (no GUI) to write the image to an SD card
* Insert SD card to your Pi and after few minutes (first time only) you'll get login prompt.
You can use `volumio` as username and password
* At this point, the new volumio system is published as ```volumio.local``` host and establish a HotSpot named ```Volumio``` with password ```volumio2``` - **connect to it**

#### Enable SSH & Modify HotSpot settings

On a browser, open ```http://volumio.local/dev``` and enable SSH

Goto ```http://volumio.local```

Navigate to Network settings:

* Change HotSpot SSID to **MOVE01xxxxx** 
* Change HotSpot password to **move123456**
* **SAVE** !!
* Connect to a WiFi network with internet connection

### Setting up the system

SSH into the device:
```
ssh volumio@volumio.local
```

#### Enabe Audio Injector

```
sudo echo 'dtoverlay=audioinjector-addons' >> /boot/config.txt
```

Reboot (You may need to reboot **twice** before it shows up)

SSH again to the device and test Audio Injector module:

```
dmesg | grep octo
```
You should get a response like so (last line is important):

```
<<<<<<< HEAD
[   11.914759] audioinjector-octo soc:sound: ASoC: CPU DAI (null) not registered - will retry
[   11.914776] audioinjector-octo soc:sound: snd_soc_register_card failed (-517)
[   13.477351] audioinjector-octo soc:sound: cs42448 <-> 3f203000.i2s mapping ok
=======
sudo echo '' > /boot/ssh
>>>>>>> d46e3936b69adea325430077eeedbc6c89e2d5fa
```


#### Selecting AudioInjector Octo as the default sink

 Open Volumio on a browser (http://volumio.local), go to settings:
 
 ![](./images/Volumio-Settings.png) 
 
 Select **PLAYBACK OPTIONS** and select **AudioInject-HIFI ...** from output devices dropdown and press **SAVE**
 
 ![](./images/Volumio-Playback-Options.png) 
 


## Testing Audio Injector

SSH into the device (if you're not already 
Check that Audio Injector is recognized:

```
dmesg | grep octo
```
```
[   11.914759] audioinjector-octo soc:sound: ASoC: CPU DAI (null) not registered - will retry
[   11.914776] audioinjector-octo soc:sound: snd_soc_register_card failed (-517)
[   13.477351] audioinjector-octo soc:sound: cs42448 <-> 3f203000.i2s mapping ok
```

#### Installing MOVE files

Update Linux Repos and install dependencies:

```
sudo apt-get update
sudo apt-get install build-essential git-core libi2c-dev i2c-tools lm-sensors libmpdclient-dev unzip
```

#### Get the files from BitBucket

```sh
cd ~
git clone https://timule@bitbucket.org/timule-MOVE/volumio-root.git
```

Stop Volumio service:

```sh
sudo su
systemctl stop volumio
```
Copy all the new files to the system (excluding media and playlists):

```sh
cd /home/volumio/volumio-root/root
cp -rvp * /
exit
```

check that we can see the anychannelcount plugin like so :
```
$ aplay -L | grep -C 1 anyCh
```
```
default
anyChannelCount
sysdefault:CARD=ALSA
```

#### Installing MOVE Hardware control program

Install of SSD1306 Driver:

```
cd ~/TRL/timule-hw-ctl/ArduiPi_SSD1306
sudo make
cd ~/TRL/timule-hw-ctl/src
rm oled
sudo make && make install
```

Test that HW program works (OLED display is running):

```
sudo oled
```

Enable it on startup:
```
cd /etc/init.d
sudo update-rc.d move-hw.sh defaults
```

#### Rename the MOVE unit

Run:
```
sudo nano /etc/hosts
```

You'll get back something like this:
```
127.0.0.1       localhost volumio 
::1             localhost ip6-localhost ip6-loopback volumio ip6-volumio
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
```

Change **volumio** to the same HotSopt name (which is MOVE S/N) - such **MOVE0100001**

Run:

```
sudo nano /etc/hostname
```
Change the name from **volumio** to **MOVE0100001**

Change password:
```
passwd
```
Old password: **volumio**
New password: **Move!2017AcTfAsT**


Now update the app (OSC server) with the same name:

Edit the file ```/volumio/osc/mainOscServer.js```
Change the line ```var bedName="????"``` to ```var bedName="MOVE0100001"```


### Copy the media files

Fastest way is to have the files on USB stick
The USB stick is already mounted on volumio at `/mnt/USB/`
Or get it from Google Drive or other repo.

Copy media files to ```/data/INTERNAL```

## Using FileZila to transfer files

Host name: **move0100004.local**
Username: **volumio**
Password: **Move!2017AcTfAsT**

Local media files are located at `/data/INTERNAL` 


## Using USB WiFi Dongle

MOVE system may experience connectivity problems when internal RPi WiFi is used since the antenna is shielded. External USB WiFi can solve the issue.
Also, the volumio back-end is using hardcoded ```wlan0``` when using WiFi and this will always be the internal Pi WiFi.

In order to use a USB WiFi dongle as `wlan0`
add new line with `dtoverlay=pi3-disable-wifi` to `/boot/config.txt`


** IMPORTANT** 
** IMPORTANT** 
** IMPORTANT** 

Don't forget to attach a WiFi dongle since after boot, the internal Pi WiFi will be disabled


## Testing that new MOVE is setup correctly

Reboot
```
sudo reboot
```

Now you can get into UI from a browser at `http://MOVE01xxxxx.local`
and SSH using:

```
ssh volumio@MOVE01xxxxx.local
```

Test playback using the APP.


## Registering new MOVE on Remot3.it

SSH into the device.

```
sudo apt-get install weavedconnectd
```
If you are using Jessie, and you have not installed the weavedconnectd package previously, you will see:

![](./images/Installing-weaved.png) 

Run weavedinstaller to configure remot3.it service attachments:
```
sudo weavedinstaller
```
After some initialization info, you will see this menu:

![](./images/setting-up-weaved-services.png) 


Sign in to our Remot3.it account (Option 1):

Username: `yossi@timule.co`
Password: `timule-move`

Enter the name of the device, same name we've used throughout the installation, such as `MOVE01xxxxx`

Now attach service for this device on Remot3.it. Select option 1 to add a service and again option 1 to choose `SSH`.
It'll now verify that you want to use the default port 22 - answer Y.
Now plugin the service name: `SSH-MOVE01xxxxx`

And you can exit now since we won't need more services at this point.

-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

















# EXPERIMENTAL

## Mounting S3 bucket

In order to use AWS S3 bucket for updates (and perhaps streaming), we want to mount it as a local drive.

Currently, the Bucket name is **timule-move** which is a unique name.

1. Remove existing package

```
sudo su
apt-get remove fuse
```

Inatall required packages:
```
apt-get install automake autotools-dev fuse g++ git libcurl4-gnutls-dev libfuse-dev libssl-dev libxml2-dev make pkg-config
```
Download and compile Fuse:

```
cd /usr/src/
wget https://github.com/libfuse/libfuse/releases/download/fuse-3.0.0/fuse-3.0.0.tar.gz
tar xzf fuse-3.0.0.tar.gz
cd fuse-3.0.0
./configure -prefix=/usr/local
make && make install
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
ldconfig
modprobe fuse
```
Download and install s3fs:
```
cd /usr/src
git clone https://github.com/s3fs-fuse/s3fs-fuse.git
cd s3fs-fuse
./autogen.sh
./configure
make
make install
exit
```

Create password file:
```
echo AWS_ACCESS_KEY_ID:AWS_SECRET_ACCESS_KEY >> /volumio/timule-move-passwd
chmod 600 /volumio/timule-move-passwd
```

Mount S3 bucket locally:
```
mkdir /volumio/s3-bucket
s3fs timule-move  /volumio/s3-bucket -o passwd_file=/volumio/timule-move-passwd
```

Now the S3 bucket is mounted locally at /volumio/s3-bucket

Mount S3 bucket on boot:
```
sudo mkdir /tmp/cache
sudo mkdir /volumio/s3-bucket
sudo chmod 777 /tmp/cache /volumio/s3-bucket
sudo nano /etc/fstab
s3fs#timule-move /volumio/s3-bucket fuse allow_other,use_cache=/tmp/cache,uid=userid,gid=groupid 0 0
mount -a
```

### Remote-IoT Registration



```
sudo apt-get update && sudo apt-get install oracle-java7-jdk
curl -s -L https://remote-iot.com/install/remote-iot-install.sh | sudo -s bash
sudo /etc/remote-iot/services/setup.sh
```
